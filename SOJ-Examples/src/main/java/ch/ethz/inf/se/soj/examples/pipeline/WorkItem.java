package ch.ethz.inf.se.soj.examples.pipeline;

import soj.annotations.Checked;

/**
 *
 * @author mschill
 */
@Checked
public class WorkItem {

	private int data;

	/**
	 * @return some data contained in the item
	 */
	public int getData() {
		return data;
	}

	/**
	 * @param data some data contained in the item
	 */
	public void setData(int data) {
		this.data = data;
	}
}
