package ch.ethz.inf.se.soj.examples;

import soj.util.concurrent.ResourceLock;
import soj.annotations.Checked;
import java.util.concurrent.locks.Condition;

/**
 *
 * @author mischael
 */
public class CarSharingExample extends Thread {

	private ResourceLock<Car> garage;
	private Condition broken, fixed;

	public CarSharingExample() {
	}

	@Override
	public void run() {
		garage = new ResourceLock<>(new Car());
		garage.lock();
		broken = garage.newCondition();
		fixed = garage.newCondition();
		(new Driver(garage, broken, fixed)).start();
		(new Driver(garage, broken, fixed)).start();
		(new Driver(garage, broken, fixed)).start();
		(new Mechanic(garage, broken, fixed)).start();
		garage.unlock();
	}
}

class Driver extends Thread {

	protected final ResourceLock<Car> garage;
	protected final Condition broken, fixed;

	public Driver(ResourceLock<Car> garage, Condition broken, Condition fixed) {
		this.garage = garage;
		this.broken = broken;
		this.fixed = fixed;
	}

	@Override
	public void run() {
		Car c = garage.getContent();

		for (int i = 0; i < 200; i++) {
			garage.lock();
			try {
				while (c.isBroken()) {
					broken.signal();
					fixed.awaitUninterruptibly();
				}
				c.drive();
			} finally {
				garage.unlock();
			}
		}
	}
}

class Mechanic extends Driver {

	public Mechanic(ResourceLock<Car> garage, Condition broken, Condition fixed) {
		super(garage, broken, fixed);
	}

	@Override
	public void run() {
		Car c = garage.getContent();
		for (int i = 0; i < 7; i++) {
			garage.lock();
			while (!c.isBroken()) {
				broken.awaitUninterruptibly();
			}
			c.fix();
			fixed.signalAll();
			c.drive();
			garage.unlock();
		}
	}
}

class Car {

	private int wear;

	public void drive() {
		wear++;
	}

	public boolean isBroken() {
		return wear > 100;
	}

	public void fix() {
		wear = 20;
	}
}
