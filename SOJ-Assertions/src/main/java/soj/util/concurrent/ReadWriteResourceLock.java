package soj.util.concurrent;

import soj.SOJ;
import soj.annotations.AbstractProcess;
import soj.annotations.Shared;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import soj.internal.Entity;
import soj.internal.FakeProcess;
import soj.internal.SharedResource;

/**
 *
 * @author mischael
 */
@AbstractProcess
public final class ReadWriteResourceLock<T> {

	private final SharedBox<T> reader;
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	@SuppressWarnings("LeakingThisInConstructor")
	public ReadWriteResourceLock(T content) {
		reader = new SharedBox<T>(content);
	}

	public T getContent() {
		return reader.getContent();
	}

	public void lockWrite() {
		lock.writeLock().lock();
		Thread t = Thread.currentThread();
		if (t instanceof Entity) {
			SOJ.pass(reader, (Entity) t);
		} else {
			SOJ.pass(reader, FakeProcess.getFakeProcess());
		}
	}

	public void unlockWrite() {
		SOJ.pass(reader, this);
		lock.writeLock().unlock();
	}

	public void lockRead() {
		lock.readLock().lock();
		((SharedResource) reader).__share(FakeProcess.getFakeProcess());
	}

	public void unlockRead() {
		((SharedResource) reader).__release();
		lock.readLock().unlock();
	}

	public boolean __isWriteable() {
		return lock.writeLock().isHeldByCurrentThread();
	}

	public boolean __isReadable() {
		return __isWriteable();
	}
}

@Shared
class SharedBox<T> {

	private final T content;

	public SharedBox(T content) {
		this.content = content;
	}

	public T getContent() {
		return content;
	}
}
