/*
 * The MIT License
 *
 * Copyright 2014 Mischael Schill <mischael.schill@inf.ethz.ch>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package soj.collections;

import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import soj.SOJ;
import soj.internal.Resource;

/**
 *
 * @author Mischael Schill <mischael.schill@inf.ethz.ch>
 */
public class SafeDeque<T> implements Deque<T> {
	private final Deque<T> original;
	private final Object source;
	
	public SafeDeque (Deque<T> original) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	public SafeDeque (Deque<T> original, Object source) {
		this.original = original;
		this.source = source;
	}

	@Override
	public void addFirst(T e) {
		SOJ.checkWrite(source);
		original.addFirst(e);
	}

	@Override
	public void addLast(T e) {
		SOJ.checkWrite(source);
		original.addLast(e);
	}

	@Override
	public boolean offerFirst(T e) {
		SOJ.checkWrite(source);
		return original.offerFirst(e);
	}

	@Override
	public boolean offerLast(T e) {
		SOJ.checkWrite(source);
		return original.offerLast(e);
	}

	@Override
	public T removeFirst() {
		SOJ.checkWrite(source);
		return original.removeFirst();
	}

	@Override
	public T removeLast() {
		SOJ.checkWrite(source);
		return original.removeLast();
	}

	@Override
	public T pollFirst() {
		SOJ.checkWrite(source);
		return original.pollFirst();
	}

	@Override
	public T pollLast() {
		SOJ.checkWrite(source);
		return original.pollLast();
	}

	@Override
	public T getFirst() {
		SOJ.checkRead(source);
		return original.getFirst();
	}

	@Override
	public T getLast() {
		SOJ.checkRead(source);
		return original.getLast();
	}

	@Override
	public T peekFirst() {
		SOJ.checkRead(source);
		return original.peekFirst();
	}

	@Override
	public T peekLast() {
		SOJ.checkRead(source);
		return original.peekLast();
	}

	@Override
	public boolean removeFirstOccurrence(Object o) {
		SOJ.checkWrite(source);
		return original.removeFirstOccurrence(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o) {
		SOJ.checkWrite(source);
		return original.removeLastOccurrence(o);
	}

	@Override
	public boolean add(T e) {
		SOJ.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean offer(T e) {
		SOJ.checkWrite(source);
		return original.offer(e);
	}

	@Override
	public T remove() {
		SOJ.checkWrite(source);
		return original.remove();
	}

	@Override
	public T poll() {
		SOJ.checkWrite(source);
		return original.poll();
	}

	@Override
	public T element() {
		SOJ.checkRead(source);
		return original.element();
	}

	@Override
	public T peek() {
		SOJ.checkRead(source);
		return original.peek();
	}

	@Override
	public void push(T e) {
		SOJ.checkWrite(source);
		original.push(e);
	}

	@Override
	public T pop() {
		SOJ.checkWrite(source);
		return original.pop();
	}

	@Override
	public boolean remove(Object o) {
		SOJ.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean contains(Object o) {
		SOJ.checkRead(source);
		return original.contains(o);
	}

	@Override
	public int size() {
		SOJ.checkRead(source);
		return original.size();
	}

	@Override
	public Iterator<T> iterator() {
		SOJ.checkRead(source);
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Iterator<T> descendingIterator() {
		SOJ.checkRead(source);
		return new SafeIterator<>(original.descendingIterator(), source);
	}

	@Override
	public boolean isEmpty() {
		SOJ.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public Object[] toArray() {
		SOJ.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		SOJ.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		SOJ.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		SOJ.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		SOJ.checkWrite(source);
		original.clear();
	}

	@Override
	public int hashCode() {
		SOJ.checkRead(source);
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		SOJ.checkRead(source);
		SOJ.checkRead(obj);
		if (obj instanceof SafeDeque) {
			SOJ.checkRead(((SafeDeque)obj).original);
			return original.equals(((SafeDeque)obj).original);
		} else return original.equals(obj);
	}

	@Override
	public String toString() {
		SOJ.checkRead(source);
		return original.toString();
	}
}
