package soj.collections;

import java.util.Collection;
import java.util.Queue;
import soj.SOJ;
import soj.internal.Resource;

/**
 *
 * @author mischael
 */
public class SafeQueue<T> implements Queue<T> {

	private final Queue<T> original;
	private final Object source;

	public SafeQueue(Queue<T> original) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	public SafeQueue(Queue<T> original, Object source) {
		this.original = original;
		this.source = source;
	}	
	
	@Override
	public int size() {
		SOJ.checkRead(source);
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		SOJ.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		SOJ.checkRead(source);
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		SOJ.checkRead(source);
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Object[] toArray() {
		SOJ.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		SOJ.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		SOJ.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		SOJ.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		SOJ.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		SOJ.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		SOJ.checkWrite(source);
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		SOJ.checkRead(source);
		SOJ.checkRead(o);
		if (o instanceof SafeQueue) {
			SafeQueue t = (SafeQueue) o;
			SOJ.checkRead(t.original);
			if (original == null) {
				return t.original == null;
			}
			return original.equals(t.original);
		} else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		SOJ.checkRead(source);
		if (original == null) {
			return super.hashCode();
		}
		return original.hashCode();
	}
	@Override
	public String toString() {
		SOJ.checkRead(source);
		if (original == null) {
			return super.toString();
		}
		return original.toString();
	}
	
	@Override
	public boolean offer(T e) {
		SOJ.checkWrite(source);
		return original.offer(e);
	}

	@Override
	public T remove() {
		SOJ.checkWrite(source);
		return original.remove();
	}

	@Override
	public T poll() {
		SOJ.checkWrite(source);
		return original.poll();
	}

	@Override
	public T element() {
		SOJ.checkRead(source);
		return original.element();
	}

	@Override
	public T peek() {
		SOJ.checkRead(source);
		return original.peek();
	}	
}
