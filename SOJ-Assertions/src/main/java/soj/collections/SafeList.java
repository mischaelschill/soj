package soj.collections;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import soj.SOJ;
import soj.internal.Resource;

/**
 *
 * @author mischael
 */
public class SafeList<T> implements List<T> {

	private final List<T> original;
	private final Object source;

	public SafeList(List<T> original) {
		assert original != null;
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	SafeList(List<T> original, Object source) {
		assert original != null;
		assert source != null;
		this.original = original;
		this.source = source;
	}
	
	@Override
	public int size() {
		SOJ.checkRead(source);
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		SOJ.checkRead(source);
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		SOJ.checkRead(source);
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<>(original.iterator(), source);
	}

	@Override
	public Object[] toArray() {
		SOJ.checkRead(source);
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		SOJ.checkRead(source);
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		SOJ.checkWrite(source);
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		SOJ.checkWrite(source);
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		SOJ.checkRead(source);
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		SOJ.checkWrite(source);
		return original.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		SOJ.checkWrite(source);
		return original.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		SOJ.checkWrite(source);
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		SOJ.checkWrite(source);
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		SOJ.checkRead(source);
		SOJ.checkRead(o);
		if (o instanceof SafeList) {
			SafeList t = (SafeList) o;
			SOJ.checkRead(t.original);
			if (original == null) {
				return t.original == null;
			}
			return original.equals(t.original);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		SOJ.checkRead(source);
		if (original == null) {
			return super.hashCode();
		}
		return original.hashCode();
	}

	@Override
	public T get(int index) {
		SOJ.checkRead(source);
		return original.get(index);
	}

	@Override
	public T set(int index, T element) {
		SOJ.checkWrite(source);
		return original.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		SOJ.checkWrite(source);
		original.add(index, element);
	}

	@Override
	public T remove(int index) {
		SOJ.checkWrite(source);
		return original.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		SOJ.checkRead(source);
		return original.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		SOJ.checkRead(source);
		return original.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		SOJ.checkRead(source);
		return new SafeListIterator<>(original.listIterator(), source);
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		SOJ.checkRead(source);
		return new SafeListIterator<>(original.listIterator(index), source);
	}

	@Override
	public SafeList<T> subList(int fromIndex, int toIndex) {
		SOJ.checkRead(source);
		return new SafeList<>(original.subList(fromIndex, toIndex), source);
	}

	@Override
	public String toString() {
		SOJ.checkRead(source);
		return original.toString();
	}

}
