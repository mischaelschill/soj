package soj.collections;

import java.util.Iterator;
import soj.SOJ;

/**
 *
 * @author mischael
 */
public class SafeIterator<T> implements Iterator<T> {
	private final Iterator<T> original;
	private final Object collection;

	public SafeIterator(Iterator<T> original, Object collection) {
		this.original = original;
		this.collection = collection;
	}

	@Override
	public boolean hasNext() {
		SOJ.checkRead (collection);
		return original.hasNext();
	}

	@Override
	public T next() {
		SOJ.checkRead (collection);
		return original.next();
	}

	@Override
	public void remove() {
		SOJ.checkWrite (collection);
		original.remove();
	}
}
