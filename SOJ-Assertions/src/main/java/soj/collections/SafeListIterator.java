package soj.collections;

import java.util.ListIterator;
import soj.SOJ;
import soj.internal.Resource;

/**
 *
 * @author mischael
 */
class SafeListIterator<T> implements ListIterator<T> {
	private final ListIterator<T> original;
	private final Object source;

	public SafeListIterator(ListIterator<T> original, Object source) {
		this.original = original;
		if (original instanceof Resource) {
			this.source = original;
		} else {
			this.source = this;
		}
	}

	@Override
	public boolean hasNext() {
		SOJ.checkRead(source);
		return original.hasNext();
	}

	@Override
	public T next() {
		SOJ.checkRead(source);
		return original.next();
	}

	@Override
	public boolean hasPrevious() {
		SOJ.checkRead(source);
		return original.hasPrevious();
	}

	@Override
	public T previous() {
		SOJ.checkRead(source);
		return original.previous();
	}

	@Override
	public int nextIndex() {
		SOJ.checkRead(source);
		return original.nextIndex();
	}

	@Override
	public int previousIndex() {
		SOJ.checkRead(source);
		return original.previousIndex();
	}

	@Override
	public void remove() {
		SOJ.checkWrite(source);
		original.remove();
	}

	@Override
	public void set(T e) {
		SOJ.checkWrite(source);
		original.set(e);
	}

	@Override
	public void add(T e) {
		SOJ.checkWrite(source);
		original.add(e);
	}

	@Override
	public int hashCode() {
		SOJ.checkRead(source);
		return original.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		SOJ.checkRead(source);
		if (obj instanceof SafeListIterator){
			SOJ.checkRead(((SafeListIterator)obj).original);
			return original.equals(((SafeListIterator)obj).original);
		} else {
			return original.equals(obj);
		}
	}

	@Override
	public String toString() {
		SOJ.checkRead(source);
		return original.toString();
	}
	
}
