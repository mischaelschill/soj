package soj.collections;

import soj.annotations.Checked;
import java.util.Collection;
import java.util.Queue;
import soj.RCL;

/**
 *
 * @author mischael
 */
@Checked
public class SafeQueue<T> implements Queue<T> {

	private final Queue<T> original;

	public SafeQueue(Queue<T> original) {
		this.original = original;
	}

	public SafeQueue(Queue<T> original, Object source) {
		this.original = original;
	}	
	
	@Override
	public int size() {
		return original.size();
	}

	@Override
	public boolean isEmpty() {
		return original.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return original.contains(o);
	}

	@Override
	public SafeIterator<T> iterator() {
		return new SafeIterator<>(original.iterator());
	}

	@Override
	public Object[] toArray() {
		return original.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return original.toArray(a);
	}

	@Override
	public boolean add(T e) {
		return original.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return original.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return original.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return original.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return original.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return original.retainAll(c);
	}

	@Override
	public void clear() {
		original.clear();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SafeQueue) {
			SafeQueue t = (SafeQueue) o;
			if (original == null) {
				return t.original == null;
			}
			return original.equals(t.original);
		} else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		if (original == null) {
			return super.hashCode();
		}
		return original.hashCode();
	}
	@Override
	public String toString() {
		if (original == null) {
			return super.toString();
		}
		return original.toString();
	}
	
	@Override
	public boolean offer(T e) {
		return original.offer(e);
	}

	@Override
	public T remove() {
		return original.remove();
	}

	@Override
	public T poll() {
		return original.poll();
	}

	@Override
	public T element() {
		return original.element();
	}

	@Override
	public T peek() {
		return original.peek();
	}	
}
